package letsgotim.com.databasetest;

import android.app.Application;

import com.squareup.okhttp.OkHttpClient;

import letsgotim.com.databasetest.DatabaseHelper.AppDb;
import letsgotim.com.databasetest.dbframework.DatabaseHelper;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class App extends Application {
    private RestAdapter restAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
        restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://trapik.acacialabs.com/api")
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        DatabaseHelper.createDatabase(this, new AppDb());
    }

    public RestAdapter getRestAdapter() {
        return restAdapter;
    }
}
