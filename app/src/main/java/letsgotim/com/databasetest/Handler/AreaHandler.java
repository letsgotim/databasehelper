package letsgotim.com.databasetest.Handler;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HNH on 7/27/2016.
 */
public class AreaHandler {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("area_id")
    @Expose
    private Integer areaId;
    @SerializedName("html_color")
    @Expose
    private String htmlColor;
    @SerializedName("line_url")
    @Expose
    private String lineUrl;
    @SerializedName("line_count")
    @Expose
    private Integer lineCount;
    @SerializedName("map_url")
    @Expose
    private String mapUrl;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The areaId
     */
    public Integer getAreaId() {
        return areaId;
    }

    /**
     *
     * @param areaId
     * The area_id
     */
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    /**
     *
     * @return
     * The htmlColor
     */
    public String getHtmlColor() {
        return htmlColor;
    }

    /**
     *
     * @param htmlColor
     * The html_color
     */
    public void setHtmlColor(String htmlColor) {
        this.htmlColor = htmlColor;
    }

    /**
     *
     * @return
     * The lineUrl
     */
    public String getLineUrl() {
        return lineUrl;
    }

    /**
     *
     * @param lineUrl
     * The line_url
     */
    public void setLineUrl(String lineUrl) {
        this.lineUrl = lineUrl;
    }

    /**
     *
     * @return
     * The lineCount
     */
    public Integer getLineCount() {
        return lineCount;
    }

    /**
     *
     * @param lineCount
     * The line_count
     */
    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    /**
     *
     * @return
     * The mapUrl
     */
    public String getMapUrl() {
        return mapUrl;
    }

    /**
     *
     * @param mapUrl
     * The map_url
     */
    public void setMapUrl(String mapUrl) {
        this.mapUrl = mapUrl;
    }

}
