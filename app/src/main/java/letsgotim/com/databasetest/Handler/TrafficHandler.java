package letsgotim.com.databasetest.Handler;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HNH on 7/27/2016.
 */
public class TrafficHandler {

    @SerializedName("areas")
    @Expose
    private List<AreaHandler> areas = new ArrayList<AreaHandler>();

    /**
     *
     * @return
     * The areas
     */
    public List<AreaHandler> getAreas() {
        return areas;
    }

    /**
     *
     * @param areas
     * The areas
     */
    public void setAreas(List<AreaHandler> areas) {
        this.areas = areas;
    }

}
