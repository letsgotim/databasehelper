package letsgotim.com.databasetest.dbframework;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.Iterator;

public class DatabaseHelper extends SQLiteOpenHelper {
    private EngineDatabase mDatabase;
    public static DatabaseHelper dbManager;

    public DatabaseHelper(Context context, EngineDatabase database) {
        super(context, database.getName(), (CursorFactory)null, database.getVersion());
        this.mDatabase = database;
    }

    public static void createDatabase(Context context, EngineDatabase database) {
        if(dbManager == null) {
            dbManager = new DatabaseHelper(context, database);
        }

    }

    public static DatabaseHelper getInstance() {
        return dbManager;
    }

    public EngineDatabase getDatabaseInfo() {
        return this.mDatabase;
    }

    public void close() {
        dbManager = null;
        super.close();
    }

    public void onCreate(SQLiteDatabase db) {
        Iterator var3 = this.mDatabase.getTables().iterator();

        while(var3.hasNext()) {
            Table table = (Table)var3.next();
            db.execSQL(table.getTableStructure());
        }

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Iterator var5 = this.mDatabase.getTables().iterator();

        while(var5.hasNext()) {
            Table table = (Table)var5.next();
            db.execSQL("DROP TABLE IF EXISTS " + table.getName());
        }

        this.onCreate(db);
    }

    public boolean copyDatabase(InputStream sourceDB, String outputPath) {
        try {
            InputStream e = sourceDB;
            FileOutputStream myOutput = new FileOutputStream(outputPath);
            byte[] buffer = new byte[1024];

            int length;
            while((length = e.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            myOutput.flush();
            myOutput.close();
            e.close();
            return true;
        } catch (IOException var8) {
            var8.printStackTrace();
            return false;
        }
    }

    public void exportDB(String dbPath, String dir) {
        File sdFile = new File(dir);
        if(!sdFile.exists()) {
            sdFile.mkdirs();
        }

        FileChannel source = null;
        FileChannel destination = null;
        String backupDBPath = this.getDatabaseName();
        File currentDB = new File(dbPath);
        File backupDB = new File(dir, backupDBPath);

        try {
            source = (new FileInputStream(currentDB)).getChannel();
            destination = (new FileOutputStream(backupDB)).getChannel();
            destination.transferFrom(source, 0L, source.size());
            source.close();
            destination.close();
            System.out.println("DB Exported");
        } catch (IOException var10) {
            var10.printStackTrace();
        }

    }
}
