package letsgotim.com.databasetest.dbframework;

import java.util.ArrayList;

public class EngineDatabase {
    private String DBName;
    private int DBVersion;
    private ArrayList<Table> tables = new ArrayList();

    public EngineDatabase(String dbname, int dbversion) {
        this.DBName = dbname;
        this.DBVersion = dbversion;
    }

    public String getName() {
        return this.DBName;
    }

    public int getVersion() {
        return this.DBVersion;
    }

    public ArrayList<Table> getTables() {
        return this.tables;
    }

    public EngineDatabase addTable(Table table) {
        this.tables.add(table);
        return this;
    }
}