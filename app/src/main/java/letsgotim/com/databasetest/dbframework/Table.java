package letsgotim.com.databasetest.dbframework;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public abstract class Table {
    private SQLiteDatabase db;

    public Table() {
    }

    public abstract String getTableStructure();

    public abstract String getName();

    public long insert(ContentValues values) {
        this.db = DatabaseHelper.getInstance().getWritableDatabase();
        return this.db != null?this.db.insertOrThrow(this.getName(), (String)null, values):-1L;
    }

    public long insertOrUpdate(ContentValues values, String filter) {
        this.db = DatabaseHelper.getInstance().getWritableDatabase();
        int rowsAffected = this.db.update(this.getName(), values, filter, (String[])null);
        return rowsAffected == 0?this.db.insertOrThrow(this.getName(), (String)null, values):(long)rowsAffected;
    }

    public int update(ContentValues values, String filter) throws SQLiteException, SQLException {
        this.db = DatabaseHelper.getInstance().getWritableDatabase();
        return this.db != null?this.db.update(this.getName(), values, filter, (String[])null):-1;
    }

    public int update(ContentValues values, String whereClause, String[] filterValues) throws SQLiteException, SQLException {
        this.db = DatabaseHelper.getInstance().getWritableDatabase();
        return this.db != null?this.db.update(this.getName(), values, whereClause, filterValues):-1;
    }

    public int delete() throws SQLiteException, SQLException {
        this.db = DatabaseHelper.getInstance().getWritableDatabase();
        return this.db != null?this.db.delete(this.getName(), (String)null, (String[])null):-1;
    }

    public int delete(String whereClause, String[] filterValues) throws SQLiteException, SQLException {
        this.db = DatabaseHelper.getInstance().getWritableDatabase();
        return this.db != null?this.db.delete(this.getName(), whereClause, filterValues):-1;
    }

    public Cursor select() throws SQLiteException, SQLException {
        this.db = DatabaseHelper.getInstance().getReadableDatabase();
        return this.db != null?this.db.rawQuery("SELECT * FROM " + this.getName(), (String[])null):null;
    }

    public Cursor select(String filter, String[] filterValues) throws SQLiteException, SQLException {
        this.db = DatabaseHelper.getInstance().getReadableDatabase();
        return this.db != null?this.db.rawQuery("SELECT * FROM " + this.getName() + " where " + filter, filterValues):null;
    }

    public Cursor rawQuery(String query, String[] filterValues) throws SQLiteException, SQLException {
        this.db = DatabaseHelper.getInstance().getReadableDatabase();
        return this.db != null?this.db.rawQuery(query, filterValues):null;
    }
}
