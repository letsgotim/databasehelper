package letsgotim.com.databasetest.Activity.Main.Presenter;

import java.util.ArrayList;

import letsgotim.com.databasetest.Handler.AreaHandler;
import letsgotim.com.databasetest.Handler.UserHandler;

public interface MainView {
    String getUsername();
    String getPassword();
    String getFullname();

    void showUsernameEmpty();

    void showPasswordEmpty();

    void showFullnameEmpty();

    void showToastMessage(String s);

    void displayUsers(ArrayList<UserHandler> users);

    void clearEntries();

    void showLoading();

    void dismissLoading();

    void displayAreas(ArrayList<AreaHandler> areaHandlers);
}
