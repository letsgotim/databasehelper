package letsgotim.com.databasetest.Activity.Main.Interactor;

public interface MainInteractor {
    void saveData(String username, String password, String fullname);

    void getData();

    void getAPIData();

    void getAreas();
}
