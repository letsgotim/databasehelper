package letsgotim.com.databasetest.Activity.Main.Interactor;

import java.util.ArrayList;

import letsgotim.com.databasetest.Activity.Main.Presenter.MainPresenter;
import letsgotim.com.databasetest.Activity.Main.Presenter.OnSaveListener;
import letsgotim.com.databasetest.Activity.Main.Presenter.OnSyncListener;
import letsgotim.com.databasetest.DatabaseHelper.Table.AreaRepo;
import letsgotim.com.databasetest.Handler.AreaHandler;
import letsgotim.com.databasetest.Handler.TrafficHandler;
import letsgotim.com.databasetest.Handler.UserHandler;
import letsgotim.com.databasetest.DatabaseHelper.Table.UserRepo;
import letsgotim.com.databasetest.WebService.TrafficAPI;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainInteractorImpl implements MainInteractor {

    private static final String TAG = "MainInteractorImpl";

    UserRepo userRepo;
    AreaRepo areaRepo;
    private OnSaveListener listener;
    private MainPresenter presenter;
    private OnSyncListener onSyncListener;
    private RestAdapter restAdapter;

    public MainInteractorImpl(OnSaveListener listener,
                              MainPresenter presenter,
                              OnSyncListener onSyncListener,
                              RestAdapter restAdapter) {
        this.onSyncListener = onSyncListener;
        this.restAdapter = restAdapter;
        this.listener = listener;
        this.presenter = presenter;
        userRepo = new UserRepo();
        areaRepo = new AreaRepo();
    }

    @Override
    public void saveData(String username, String password, String fullname) {
        userRepo = new UserRepo();
        if (userRepo.addUser(new UserHandler(username, password, fullname)) > 0) {
            listener.onSaveSuccess();
        } else {
            listener.onSaveFail();
        }
        presenter.requestRetrieveData();
        presenter.requestClearEntries();
    }

    @Override
    public void getData() {
        ArrayList<UserHandler> users = new ArrayList<>();
        users = userRepo.getAllUsers();
        presenter.returnData(users);
    }

    @Override
    public void getAPIData() {
        TrafficAPI trafficAPI = restAdapter.create(TrafficAPI.class);
        trafficAPI.getTraffic(new Callback<TrafficHandler>() {
            @Override
            public void success(TrafficHandler trafficHandler, Response response) {
                for (AreaHandler data : trafficHandler.getAreas()) {
                    areaRepo.addArea(data);
                }
                onSyncListener.onSyncSuccess();
            }

            @Override
            public void failure(RetrofitError error) {
                onSyncListener.onSyncFail();
            }
        });
    }

    @Override
    public void getAreas() {
        ArrayList<AreaHandler> areaHandlers = new ArrayList<>();
        areaHandlers = areaRepo.getAllUsers();
        presenter.returnAreas(areaHandlers);
    }
}
