package letsgotim.com.databasetest.Activity.Main.Presenter;

public interface OnSyncListener {
    void onSyncSuccess();
    void onSyncFail();
}
