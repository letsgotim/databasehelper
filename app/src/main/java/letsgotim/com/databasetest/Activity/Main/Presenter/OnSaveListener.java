package letsgotim.com.databasetest.Activity.Main.Presenter;

public interface OnSaveListener {
    void onSaveSuccess();
    void onSaveFail();
}
