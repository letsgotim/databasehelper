package letsgotim.com.databasetest.Activity.Main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import letsgotim.com.databasetest.Activity.Main.Presenter.ClickListener;
import letsgotim.com.databasetest.Activity.Main.Presenter.MainPresenterImpl;
import letsgotim.com.databasetest.Activity.Main.Presenter.MainView;
import letsgotim.com.databasetest.Adapter.UserAdapter;
import letsgotim.com.databasetest.App;
import letsgotim.com.databasetest.DividerItemDecoration;
import letsgotim.com.databasetest.Handler.AreaHandler;
import letsgotim.com.databasetest.Handler.UserHandler;
import letsgotim.com.databasetest.R;

public class MainActivity extends AppCompatActivity implements MainView {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txt_username)
    EditText txtUsername;
    @Bind(R.id.txt_password)
    EditText txtPassword;
    @Bind(R.id.txt_fullname)
    EditText txtFullname;
    @Bind(R.id.btn_save)
    Button btnSave;
    @Bind(R.id.llt_user_info)
    LinearLayout lltUserInfo;

    @Bind(R.id.btn_api)
    Button btnApi;
    @Bind(R.id.rcView)
    RecyclerView rcView;

    private MainPresenterImpl presenter;
    private ProgressDialog progressDialog;
    private UserAdapter userAdapter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
    }

    public App getApp() {
        return (App) this.getApplication();
    }

    private void init() {
        context = this;
        setSupportActionBar(toolbar);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setTitle("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        rcView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rcView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ArrayList<UserHandler> users = userAdapter.getUsers();
                if (position != 0)
                    Toast.makeText(context, "Username " + users.get(position - 1).getUsername() +
                            " Password " + users.get(position - 1).getPassword(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        presenter = new MainPresenterImpl(this, getApp().getRestAdapter(), this);
        presenter.requestRetrieveData();

    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;

            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_data:
                presenter.requestAreas();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_save)
    void clickSave() {
        presenter.requestSaveData();
    }

    @OnClick(R.id.btn_api)
    void clickAPI() {
        presenter.requestAPIData();
    }

    /****
     * GET
     *****/

    @Override
    public String getUsername() {
        return txtUsername.getText().toString();
    }

    @Override
    public String getPassword() {
        return txtPassword.getText().toString();
    }

    @Override
    public String getFullname() {
        return txtFullname.getText().toString();
    }

    /******
     * DISPLAY
     *******/

    @Override
    public void showUsernameEmpty() {
        txtUsername.setError("Username is empty");
    }

    @Override
    public void showPasswordEmpty() {
        txtUsername.setError("Password is empty");
    }

    @Override
    public void showFullnameEmpty() {
        txtUsername.setError("Fullname is empty");
    }

    @Override
    public void showToastMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void displayUsers(ArrayList<UserHandler> users) {
        userAdapter = new UserAdapter(users);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rcView.setLayoutManager(layoutManager);
        rcView.setItemAnimator(new DefaultItemAnimator());
        rcView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rcView.setAdapter(userAdapter);
    }

    @Override
    public void clearEntries() {
        txtUsername.setText("");
        txtPassword.setText("");
        txtFullname.setText("");
        txtUsername.requestFocus();
    }

    @Override
    public void showLoading() {
        progressDialog.show();
    }

    @Override
    public void dismissLoading() {
        progressDialog.dismiss();
    }

    @Override
    public void displayAreas(ArrayList<AreaHandler> areaHandlers) {
        ArrayList<String> areas = new ArrayList<>();
        for (AreaHandler data : areaHandlers) {
            areas.add(data.getName() + " - " + data.getLineUrl());
        }
        new MaterialDialog.Builder(this)
                .title("Select Area")
                .items(areas)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        Toast.makeText(MainActivity.this, text.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .negativeText("Cancel")
                .show();
    }
}
