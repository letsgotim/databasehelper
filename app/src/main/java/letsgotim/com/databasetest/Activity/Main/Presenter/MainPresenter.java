package letsgotim.com.databasetest.Activity.Main.Presenter;

import java.util.ArrayList;

import letsgotim.com.databasetest.Handler.AreaHandler;
import letsgotim.com.databasetest.Handler.UserHandler;

public interface MainPresenter {
    void requestSaveData();
    void requestRetrieveData();

    void returnData(ArrayList<UserHandler> users);

    void requestAPIData();

    void requestClearEntries();

    void requestAreas();

    void returnAreas(ArrayList<AreaHandler> areaHandlers);
}
