package letsgotim.com.databasetest.Activity.Main.Presenter;

import android.content.Context;

import java.util.ArrayList;

import letsgotim.com.databasetest.Activity.Main.Interactor.MainInteractorImpl;
import letsgotim.com.databasetest.Handler.AreaHandler;
import letsgotim.com.databasetest.Handler.UserHandler;
import letsgotim.com.databasetest.Utility;
import retrofit.RestAdapter;

public class MainPresenterImpl implements MainPresenter, OnSaveListener, OnSyncListener {

    private final MainInteractorImpl interactor;
    private MainView mainView;
    private Context context;

    public MainPresenterImpl(MainView mainView, RestAdapter restAdapter, Context context) {
        this.mainView = mainView;
        this.context = context;
        interactor = new MainInteractorImpl(this, this, this, restAdapter);
    }

    @Override
    public void requestAreas() {
        interactor.getAreas();
    }

    /*********
     * REQUEST
     */


    @Override
    public void requestAPIData() {
        if (Utility.isNetworkAvailable(context)) {
            mainView.showLoading();
            interactor.getAPIData();
        } else {
            mainView.showToastMessage("Please check internet connection");
        }
    }

    @Override
    public void requestSaveData() {
        String username = mainView.getUsername();
        String password = mainView.getPassword();
        String fullname = mainView.getFullname();

        if (username.isEmpty()) {
            mainView.showUsernameEmpty();
            return;
        }

        if (password.isEmpty()) {
            mainView.showPasswordEmpty();
            return;
        }

        if (fullname.isEmpty()) {
            mainView.showFullnameEmpty();
            return;
        }

        interactor.saveData(username, password, fullname);
    }

    @Override
    public void requestRetrieveData() {
        interactor.getData();
    }

    @Override
    public void requestClearEntries() {
        mainView.clearEntries();
    }

    /*********
     * SAVE LISTENER
     */

    @Override
    public void onSaveSuccess() {
        mainView.showToastMessage("Save Complete");
    }

    @Override
    public void onSaveFail() {
        mainView.showToastMessage("Save Fail");
    }

    /***********
     * SYNC LISTENER
     */

    @Override
    public void onSyncSuccess() {
        mainView.dismissLoading();
        mainView.showToastMessage("Sync Complete");
    }

    @Override
    public void onSyncFail() {
        mainView.dismissLoading();
        mainView.showToastMessage("Sync Fail");
    }


    /************
     * RETURN
     */

    @Override
    public void returnAreas(ArrayList<AreaHandler> areaHandlers) {
        mainView.displayAreas(areaHandlers);
    }

    @Override
    public void returnData(ArrayList<UserHandler> users) {
        mainView.displayUsers(users);
    }
}
