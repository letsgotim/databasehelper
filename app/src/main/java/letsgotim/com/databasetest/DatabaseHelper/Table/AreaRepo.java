package letsgotim.com.databasetest.DatabaseHelper.Table;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;

import letsgotim.com.databasetest.Handler.AreaHandler;
import letsgotim.com.databasetest.Handler.UserHandler;
import letsgotim.com.databasetest.dbframework.Table;

public class AreaRepo extends Table {

    public static final String TAG = "AreaRepo";
    private static final String ID = "id";
    private static final String AREA_ID = "area_id";
    private static final String AREA_NAME = "name";
    private static final String AREA_HTML_COLOR = "html_color";
    private static final String AREA_LINE_URL = "line_url";
    private static final String AREA_LINE_COUNT = "line_count";
    private static final String AREA_MAP_URL = "map_url";

    public static final String TABLE_AREA = "area_tbl";

    public static final String TABLE_STRUCTURE = "CREATE TABLE " + TABLE_AREA + " ( " +
            ID + " INTEGER PRIMARY KEY, " +
            AREA_ID + " TEXT, " +
            AREA_NAME + " TEXT, " +
            AREA_HTML_COLOR + " TEXT, " +
            AREA_LINE_URL + " TEXT, " +
            AREA_LINE_COUNT + " TEXT, " +
            AREA_MAP_URL + " TEXT, " +
            "UNIQUE ( " + AREA_ID + ") ON CONFLICT REPLACE);";

    public AreaHandler parseArea(Cursor cursor) {
        AreaHandler data = new AreaHandler();
        data.setAreaId(cursor.getInt(cursor.getColumnIndex(AREA_ID)));
        data.setName(cursor.getString(cursor.getColumnIndex(AREA_NAME)));
        data.setHtmlColor(cursor.getString(cursor.getColumnIndex(AREA_HTML_COLOR)));
        data.setLineUrl(cursor.getString(cursor.getColumnIndex(AREA_LINE_URL)));
        data.setLineCount(cursor.getInt(cursor.getColumnIndex(AREA_LINE_COUNT)));
        data.setMapUrl(cursor.getString(cursor.getColumnIndex(AREA_MAP_URL)));

        return data;
    }


    public boolean isAreaTablePopulated() {
        String query = "SELECT * FROM " + getName();
        Cursor cursor = rawQuery(query, null);

        if (cursor.getCount() == 0) {
            return false;
        } else {
            return true;
        }
    }

    public long addArea(AreaHandler data) {
        ContentValues values = new ContentValues();
        values.put(AREA_ID, data.getAreaId());
        values.put(AREA_NAME, data.getName());
        values.put(AREA_HTML_COLOR, data.getHtmlColor());
        values.put(AREA_LINE_URL, data.getLineUrl());
        values.put(AREA_LINE_COUNT, data.getLineCount());
        values.put(AREA_MAP_URL, data.getMapUrl());
        return insert(values);
    }

    public ArrayList<AreaHandler> getAllUsers() {
        String query = "SELECT * FROM " + getName();
        Cursor cursor = rawQuery(query, null);
        ArrayList<AreaHandler> areas = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    AreaHandler data = parseArea(cursor);
                    areas.add(data);
                    cursor.moveToNext();
                }
            }
        }
        return areas;
    }


    @Override
    public String getTableStructure() {
        return TABLE_STRUCTURE;
    }

    @Override
    public String getName() {
        return TABLE_AREA;
    }
}
