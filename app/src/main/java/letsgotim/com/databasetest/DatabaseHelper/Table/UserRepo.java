package letsgotim.com.databasetest.DatabaseHelper.Table;

import android.content.ContentValues;
import android.database.Cursor;


import java.util.ArrayList;

import letsgotim.com.databasetest.Handler.UserHandler;
import letsgotim.com.databasetest.dbframework.Table;

public class UserRepo extends Table {

    public static final String TAG = "UserRepo";
    private static final String USER_ID = "id";
    private static final String USER_USERNAME = "username";
    private static final String USER_PASSWORD = "password";
    private static final String USER_FULLNAME = "fullname";

    public static final String TABLE_USER = "user_tbl";

    public static final String TABLE_STRUCTURE = "CREATE TABLE " + TABLE_USER + " ( " +
            USER_ID + " INTEGER PRIMARY KEY, " +
            USER_USERNAME + " TEXT, " +
            USER_PASSWORD + " TEXT, " +
            USER_FULLNAME + " TEXT, " +
            "UNIQUE ( " + USER_ID + ") ON CONFLICT REPLACE);";

    public UserHandler parseUser(Cursor cursor) {

        UserHandler data = new UserHandler();
        data.setId(cursor.getString(cursor.getColumnIndex(USER_ID)));
        data.setUsername(cursor.getString(cursor.getColumnIndex(USER_USERNAME)));
        data.setPassword(cursor.getString(cursor.getColumnIndex(USER_PASSWORD)));
        data.setFullname(cursor.getString(cursor.getColumnIndex(USER_FULLNAME)));

        return data;
    }


    public boolean isUserTablePopulated() {
        String query = "SELECT * FROM " + TABLE_USER;
        Cursor cursor = rawQuery(query, null);

        if (cursor.getCount() == 0) {
            return false;
        } else {
            return true;
        }
    }

    public long addUser(UserHandler data) {
        ContentValues values = new ContentValues();
        values.put(USER_ID, data.getId());
        values.put(USER_USERNAME, data.getUsername());
        values.put(USER_PASSWORD, data.getPassword());
        values.put(USER_FULLNAME, data.getFullname());

        return insert(values);
    }

    public ArrayList<UserHandler> getAllUsers() {
        String query = "SELECT * FROM " + getName();
        Cursor cursor = rawQuery(query, null);
        ArrayList<UserHandler> UserList = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    UserHandler data = parseUser(cursor);
                    UserList.add(data);
                    cursor.moveToNext();
                }
            }
        }
        return UserList;
    }


    @Override
    public String getTableStructure() {
        return TABLE_STRUCTURE;
    }

    @Override
    public String getName() {
        return TABLE_USER;
    }
}
