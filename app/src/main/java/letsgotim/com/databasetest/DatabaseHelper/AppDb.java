package letsgotim.com.databasetest.DatabaseHelper;

import letsgotim.com.databasetest.DatabaseHelper.Table.AreaRepo;
import letsgotim.com.databasetest.DatabaseHelper.Table.UserRepo;
import letsgotim.com.databasetest.dbframework.EngineDatabase;

public class AppDb extends EngineDatabase {
    public final static String DB_NAME = "test";
    public final static int DB_VERSION = 1;

    public AppDb() {
        super(DB_NAME, DB_VERSION);
        addTable(new UserRepo());
        addTable(new AreaRepo());
    }
}
