package letsgotim.com.databasetest.WebService;

import letsgotim.com.databasetest.Handler.TrafficHandler;
import retrofit.Callback;
import retrofit.http.GET;

public interface TrafficAPI {
    @GET("/traffic")
    void getTraffic(Callback<TrafficHandler> callback);
}
