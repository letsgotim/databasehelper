package letsgotim.com.databasetest.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import letsgotim.com.databasetest.Handler.UserHandler;
import letsgotim.com.databasetest.R;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private static final int TYPE_HEAD = 0;
    private static final int TYPE_LIST = 1;

    private ArrayList<UserHandler> areas;

    public UserAdapter(ArrayList<UserHandler> areas) {
        this.areas = areas;
    }

    public ArrayList<UserHandler> getUsers(){
        return areas;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        int viewType;

        //Header
        TextView lblFullname;

        //List
        TextView lblName;

        public MyViewHolder(View view, int viewType) {
            super(view);

            if (viewType == TYPE_LIST) {
                lblName = (TextView) view.findViewById(R.id.lbl_name);
                this.viewType = 1;
            } else if (viewType == TYPE_HEAD) {
                lblFullname = (TextView) view.findViewById(R.id.lbl_fullname);
                this.viewType = 0;
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        MyViewHolder myViewHolder;

        if (viewType == TYPE_LIST) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_row, parent, false);
            myViewHolder = new MyViewHolder(view, viewType);
            return myViewHolder;
        } else if (viewType == TYPE_HEAD) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_header_list_row, parent, false);
            myViewHolder = new MyViewHolder(view, viewType);
            return myViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        UserHandler user;

        if (holder.viewType == TYPE_LIST) {
            user = areas.get(position - 1);
            holder.lblName.setText(user.getFullname());
        } else if (holder.viewType == TYPE_HEAD) {
            holder.lblFullname.setText("Full name");
        }
    }

    @Override
    public int getItemCount() {
        return areas.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEAD;
        return TYPE_LIST;
    }
}